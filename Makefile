CC = gcc
CFLAGS = -std=gnu99 -g -Wall -I$(INC_DIR):$(FFMPEG_INC_DIR) -L~/ffmpeg_build/lib
FFMPEG_INC_DIR = ~/ffmpeg_build/include
INC_DIR = include
SRC_DIR = src
OBJ_DIR = obj
BIN_DIR = bin
LDFLAGS = -lpthread -lavutil -lavformat -lavcodec -lswscale -lz -lm

STREAM_CLIENT = stream_client
STREAM_SERVER = stream_server
HEADER = $(patsubst %, $(INC_DIR)/%, $(_HEADER))
_HEADER = packet_queue.h socket.h stream_server.h
STREAM_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_STREAM_OBJ))
_STREAM_OBJ = packet_queue.o socket.o stream_server.o udp_stream.o

all: dirs $(addprefix bin/, $(STREAM_SERVER))

dirs:
	mkdir -p obj
	mkdir -p bin

$(STREAM_CLIENT): $(SRC_DIR)/%.c
	$(CC) -o $@ $< $(CFLAGS) $(LDFLAGS)

$(BIN_DIR)/$(STREAM_SERVER): $(STREAM_OBJ) $(HEADER)
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(INC_DIR)/%.h
	$(CC) -o $@ -c $< $(CFLAGS) $(LDFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean
clean:
	rm -f $(BIN_DIR)/$(STREAM_SERVER)
	rm -rf $(OBJ_DIR)/*
