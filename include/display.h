#ifndef DISPLAY_H
#define DISPLAY_H

#include <SDL/SDL.h>

#define WIDTH 640
#define HEIGHT 480
#define DEPTH 32

typedef struct
{
    uint8_t r;
    uint8_t g;
    uint8_t b;

} color;

Uint32 getpixel(SDL_Surface *surface, int x, int y);
void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
void DrawScreen(SDL_Surface* screen, Uint8 *pFrame);
int Init_SDL(SDL_Surface *screen);

#endif
