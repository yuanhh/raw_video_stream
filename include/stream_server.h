#ifndef STREAM_SERVER_H
#define STREAM_SERVER_H

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>

#include "socket.h"
#include "packet_queue.h"

typedef struct video_state {

    AVFormatContext *oc;
    AVCodecContext *avctx;

    int video_st, audio_st;

    PacketQueue videoq;

    // this is for new ffmpeg lib api
    // avcodec_send_packet & avcodec_receive_frame
    // pthread_t feeding_thread;
    // pthread_t draining_thread;
    // pthread_mutex_t video_mutex;
    // pthread_cond_t video_cond;

    pthread_t decode_thread;

    int sock_fd;
    struct sockaddr *cli;

    double video_clock;

} videostate;

typedef struct Stream_header {
    int width;
    int height;
    double pts;
    double frame_delay;
} streamhdr;

AVFrame *new_RGBframe(int width, int height);
double synchronize_video(AVFrame *pFrame, videostate *is);
int send_frame(AVFrame *pFrame, videostate *is, struct SwsContext *sws_ctx);
int video_stream(char *filename, int fd, struct sockaddr *client);
void *draining(void *arg);
void *feeding(void *arg);
int stream_component_open(videostate *is);
void find_stream_index(const AVFormatContext *pFormatCtx, int *video_st, int *audio_st);
int retrieve_stream_info(AVFormatContext *oc, char *name, int *video_st, int *audio_st);
void stream_init(videostate *is, int fd, struct sockaddr *cli);

#endif
