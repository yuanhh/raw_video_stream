#include "../include/display.h"

Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;

    Uint8 *pixel = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch (bpp)
    {
        case 1:
            return *pixel;
            break;

        case 2:
            return *(Uint16 *)pixel;
            break;

        case 3:
            if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
                return pixel[0] << 16 | pixel[1] << 8 | pixel[2];
            else 
                return pixel[0] | pixel[1] << 8 | pixel[2] << 16;
            break;

        case 4:
            return *(Uint32 *)pixel;
            break;

        default:
            return 0;
    }
}

void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    int bpp = surface->format->BytesPerPixel;

    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch (bpp)
    {
        case 1:
            *p = pixel;
            break;

        case 2:
            *(Uint16 *)p = pixel;
            break;

        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            {
                p[0] = (pixel >> 16) & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = pixel & 0xff;
            }
            else
            {
                p[0] = pixel & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = (pixel >> 16) & 0xff;
            }
            break;

        case 4:
            *(Uint32 *)p = pixel;
            break;
    }
}

void DrawScreen(SDL_Surface* screen, Uint8 *pFrame)
{ 
    int x, y;
    color pixel;
    Uint8 *p = pFrame;

    if (SDL_MUSTLOCK(screen)) 
    {
        if (SDL_LockSurface(screen) < 0) return;
    }

    for (y = 0; y < screen->h; y++ ) 
    {
        for (x = 0; x < screen->w; x++) 
        {
            pixel.r = *p++;
            pixel.g = *p++;
            pixel.b = *p++;
            putpixel(screen, x, y, 
                    SDL_MapRGB(screen->format, pixel.r, pixel.g, pixel.b));
        }
    }

    if (SDL_MUSTLOCK(screen)) SDL_UnlockSurface(screen);

    SDL_Flip(screen); 
}

int Init_SDL(SDL_Surface *screen)
{
    if (SDL_Init( SDL_INIT_VIDEO  ) == -1)
    {
        fprintf(stderr, "Could not initialize SDL - %s\n", SDL_GetError());
        return -1;
    }
    if (!(screen = SDL_SetVideoMode(WIDTH, HEIGHT, DEPTH, SDL_DOUBLEBUF | SDL_HWSURFACE)))
    {
        SDL_Quit();
        return -1;
    }
    return 0;
}

