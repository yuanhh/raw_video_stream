#define _POSIX_SOURCE
#include <dirent.h>
#include <errno.h>
#undef _POSIX_SOURCE
#include <stdio.h>
#include <stdint.h>     /* uint8_t */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stddef.h>     /* offsetof() */
#include <sys/select.h> /* select() */
#include <sys/wait.h>
#include <sys/types.h>  /* pid */
#include <sys/socket.h> /* sendto() */
#include <netinet/in.h> /* struct sockaddr_in */

#include "../include/stream_server.h"

#define BUFFERSIZE 1024
#define PORT "8000"

int main(int argc, char *argv[])
{
    int listen_fd = 0;
    unsigned char buffer[BUFFERSIZE] = {0};

    if (argc != 2)
    {
        fprintf(stderr, "usage: %s [port] [video_filename]\n", argv[0]);
        return -1;
    }

    listen_fd = passive_socket(PORT, "udp", 0);
    printf("listen on port: 8000\n");

    for (;;)
    {
        memset(buffer, 0, BUFFERSIZE);

        int status = 0;
        pid_t pid;

        struct sockaddr_in cli;
        socklen_t len;

        recvfrom(listen_fd, buffer, BUFFERSIZE, 0,
                (struct sockaddr *)&cli, &len);

        pid = fork();
        if (pid == 0)
        {
            /* This is the child process. */
            status = video_stream(argv[1], listen_fd, (struct sockaddr *)&cli);
            if (status < 0)
            {
                exit(EXIT_FAILURE);
            }
            else
                exit(EXIT_SUCCESS);
        }
        else if (pid < 0)
        {
            /* The fork failed.  Report failure.  */
            status = -1;
        }
        else
        {
            /* This is the parent process. */
        }
    }
    return 0;
}
