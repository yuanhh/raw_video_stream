#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/avstring.h>
#include <libavutil/imgutils.h>

#include "../include/stream_server.h"

#define MAX_VIDEOQ_SIZE (5 * 256 * 1024)
#define UDP_PAYLOAD_SIZE 512

int quit = 0;

AVFrame *new_RGBframe(int width, int height)
{
    AVFrame *frame = av_frame_alloc();
    uint8_t *buffer;
    int numBytes;

    // Determine required buffer size and allocate buffer
    numBytes = av_image_get_buffer_size(AV_PIX_FMT_RGB24, width, height, 1);
    buffer = (uint8_t *)av_malloc(numBytes * sizeof(uint8_t));

    // Assign appropriate parts of buffer to image planes in pFrameRGB
    // Note that pFrameRGB is an AVFrame, but AVFrame is a superset
    // of AVPicture
    av_image_fill_arrays(frame->data, frame->linesize, buffer, AV_PIX_FMT_RGB24,
            width, height, 1);

    return frame;
}

double synchronize_video(AVFrame *pFrame, videostate *is)
{
    double pts = 0;
    double frame_delay;

    if ((pts = av_frame_get_best_effort_timestamp(pFrame)) == AV_NOPTS_VALUE)
    {
        pts = 0;
    }
    pts *= av_q2d(is->avctx->time_base);

    if (pts != 0)
    {
        is->video_clock = pts;
    }
    else
    {
        pts = is->video_clock;
    }

    /* update the video clock */
    frame_delay = av_q2d(is->avctx->time_base);
    /* if we are repeating a frame, adjust clock accordingly */
    frame_delay += pFrame->repeat_pict * (frame_delay * 0.5);
    is->video_clock += frame_delay;

    return pts;
}

int send_frame(AVFrame *pFrame, videostate *is, struct SwsContext *sws_ctx)
{
    AVFrame *pFrameRGB = new_RGBframe(is->avctx->width, is->avctx->height);
    streamhdr message;
    unsigned char *buffer = NULL;
    int len;

    // convert frame to rgb format
    sws_scale(sws_ctx, (uint8_t const * const *)pFrame->data, pFrame->linesize,
            0, is->avctx->height, pFrameRGB->data, pFrameRGB->linesize);

    message.width = is->avctx->width;
    message.height = is->avctx->height;
    message.pts = synchronize_video(pFrameRGB, is);
    message.frame_delay = is->video_clock - message.pts;

    len = sizeof(message) + (message.width * 3 + message.height);
    buffer = (unsigned char *)calloc(len, sizeof(unsigned char));

    memmove(buffer, &message, sizeof(message));
    memmove(buffer + sizeof(message), pFrameRGB->data[0], (message.width * 3 + message.height));
    sendto(is->sock_fd, buffer, len, 0, is->cli, sizeof(*(is->cli)));

    free(buffer);
    av_frame_free(&pFrameRGB);

    return 0;
}

void *decode_loop(void *arg)
{
    videostate *is = arg;
    struct SwsContext *sws_ctx = NULL;
    AVPacket packet, *pkt = &packet;
    AVFrame *pFrame;
    int ret, got_frame = 0;

    pFrame = av_frame_alloc();
    if (!pFrame)
    {
        pthread_exit(NULL);
    }

    sws_ctx = sws_getContext(is->avctx->width, is->avctx->height,
            is->avctx->pix_fmt, is->avctx->width, is->avctx->height,
            AV_PIX_FMT_RGB24, SWS_BILINEAR, NULL, NULL, NULL);

    while ((ret = packet_queue_get(&is->videoq, pkt, 1)) < 0)
    {
        ret = avcodec_decode_video2(is->avctx, pFrame, &got_frame, pkt);
        if (ret < 0)
        {
            fprintf(stderr, "Error while decoding frame!\n");
            pthread_exit(NULL);
        }
        else if (got_frame)
        {
            send_frame(pFrame, is, sws_ctx);
        }
        else continue;
    }
    return 0;

    av_frame_free(&pFrame);
}

int stream_component_open(videostate *is)
{
    AVCodecContext *pCodecOrig, *c = NULL;
    AVCodec *codec = NULL;

    if ((unsigned int)is->video_st >= is->oc->nb_streams)
    {
        return -1;
    }
    pCodecOrig = is->oc->streams[is->video_st]->codec;

    // Find the decoder for the video stream
    codec = avcodec_find_decoder(pCodecOrig->codec_id);
    if(!codec)
    {
        fprintf(stderr, "Unsupported codec!\n");
        return -1;
    }

    // copy context
    c = avcodec_alloc_context3(codec);
    if (avcodec_copy_context(c, pCodecOrig) < 0)
    {
        fprintf(stderr, "Couldn't copy codec context");
        return -1;
    }

    // Open codec
    if(avcodec_open2(c, codec, NULL) < 0)
    {
        fprintf(stderr, "Could not open codec!\n");
        return -1;
    }
    is->avctx = c;

    packet_queue_init(&is->videoq);
    if (pthread_create(&is->decode_thread, NULL, &decode_loop, is))
    {
        perror("pthread_create() error");
        return -1;
    }
    return 0;
}

void find_stream_index(const AVFormatContext *pFormatCtx, int *video_st, int *audio_st)
{
    unsigned int index = 0;

    *video_st = -1;
    *audio_st = -1;
    for (index = 0; index < pFormatCtx->nb_streams; ++index)
    {
        if (pFormatCtx->streams[index]->codecpar->codec_type ==
                AVMEDIA_TYPE_VIDEO)
        {
            *video_st = index;
        }
        else if (pFormatCtx->streams[index]->codecpar->codec_type ==
                AVMEDIA_TYPE_AUDIO)
        {
            *audio_st = index;
        }
    }
    return;
}

int retrieve_stream_info(AVFormatContext *oc, char *name, int *video_st, int *audio_st)
{
    // Retrieve stream information
    if(avformat_find_stream_info(oc, NULL)<0)
    {
        fprintf(stderr, "Counldn't find stream information!\n");
        return -1;
    }

    // Dump information about file onto standard error
    av_dump_format(oc, 0, name, 0);

    // Find the first video stream
    find_stream_index(oc, video_st, audio_st);
    if (video_st < 0 || audio_st < 0)
    {
        fprintf(stderr, "Didn't find a video stream!\n");
        return -1;
    }
    return 0;
}

void stream_init(videostate *is, int fd, struct sockaddr *cli)
{
    // Register all formats and codecs
    av_register_all();

    is = av_mallocz(sizeof(videostate));

    is->sock_fd = fd;
    is->cli = cli;
}

int video_stream(char *filename, int fd, struct sockaddr *client)
{
    // Initalizing these to NULL prevents segfaults!
    videostate *is = NULL;
    AVPacket packet, *pkt = &packet;
    int ret = 0;

    stream_init(is, fd, client);

    // Open video file
    if(avformat_open_input(&is->oc, filename, NULL, NULL) != 0)
    {
        fprintf(stderr, "Couldn't open file!\n");
        return -1;
    }
    // retrieve stream information
    ret = retrieve_stream_info(is->oc, filename, &is->video_st, &is->audio_st);
    if (ret < 0) 
    {
        return -1;
    }

    ret = stream_component_open(is);
    if (ret < 0)
    {
        return -1;
    }

    while (!quit)
    {
        if (is->videoq.size > MAX_VIDEOQ_SIZE)
        {
            usleep(1000);
            continue;
        }
        if (av_read_frame(is->oc, pkt) < 0)
        {
            if (is->oc->pb->error == 0)
            {
                usleep(10000);
                continue;
            }
            else break;
        }
        if (pkt->stream_index == is->video_st)
        {
            packet_queue_put(&is->videoq, pkt);
        }
        else
        {
            av_packet_unref(pkt);
        }
    }

    if(pthread_join(is->decode_thread, NULL))
    {
        fprintf(stderr, "Error joining thread\n");
        exit(EXIT_FAILURE);
    }

    // close video input
    avformat_close_input(&is->oc);
    av_free(is);

    return 0;
}
