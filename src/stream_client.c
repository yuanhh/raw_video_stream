#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>

#include "../include/display.h"
#include "../include/socket.h"

int main(int argc, char* argv[])
{
    SDL_Surface *screen;
    SDL_Event event;
    int sock_fd;

    int keypress = 0;

    if (argc != 3)
    {
        fprintf(stderr, "Usage: %s <server ip> <server port>", argv[0]);
        return -1;
    }

    if (Init_SDL(screen) < 0)
    {
        return -1;
    }
    sock_fd = connect_socket(argv[1], argv[2]);


    while(!keypress) 
    {
        recvfrom()
        DrawScreen(screen, buffer);
        while(SDL_PollEvent(&event)) 
        {      
            switch (event.type) 
            {
                case SDL_QUIT:
                    keypress = 1;
                    break;
                case SDL_KEYDOWN:
                    keypress = 1;
                    break;
            }
        }
    }

    SDL_Quit();

    return 0;

}
