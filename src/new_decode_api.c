
typedef int (*process_frame_cb)(void *ctx, AVFrame *frame);

int packet_msg(int ret)
{
    switch (ret)
    {
        case AVERROR(EAGAIN):
            fprintf(stderr, "input/output is not accepted right now\n");
            break;
        case AVERROR(EINVAL):
            fprintf(stderr, "codec not opened\n");
            return -1;
            break;
        case AVERROR(ENOMEM):
            fprintf(stderr, "legitimate decoding errors\n");
            return -1;
            break;
    }
    return 0;
}

int decode(AVCodecContext *avctx, AVPacket *pkt, process_frame_cb cb, void *priv)
{
    AVFrame *frame = av_frame_alloc();
    int ret;

    ret = avcodec_send_packet(avctx, pkt);
    if (ret< 0)
    {
        goto out;
    }

    while (!ret)
    {
        ret = avcodec_receive_frame(avctx, frame);
        if (!ret)
        {
            ret = cb(priv, frame);
        }
    }

out:
    av_frame_free(&frame);
    if (packet_msg(ret) < 0)
        return ret;
    return 0;
} 

void *draining(void *arg)
{
    videostate *is = arg;
    AVFrame *frame = av_frame_alloc();
    struct SwsContext *sws_ctx = NULL;
    int ret = 0;

    sws_ctx = sws_getContext(is->avctx->width, is->avctx->height,
            is->avctx->pix_fmt, is->avctx->width, is->avctx->height,
            AV_PIX_FMT_RGB24, SWS_BILINEAR, NULL, NULL, NULL);

    while (!quit)
    {
        pthread_mutex_lock(&is->video_mutex);

        ret = avcodec_receive_frame(is->avctx, frame);
        if (!ret)
        {
            pthread_cond_signal(&is->video_cond);
        }
        else if (ret == AVERROR(EAGAIN))
        {
            // Signal the feeding loop
            pthread_cond_signal(&is->video_cond);
            // Wait
            pthread_cond_wait(&is->video_cond, &is->video_mutex);
        }
        else if (ret < 0)
        {
            pthread_mutex_unlock(&is->video_mutex);
            return 0;
        }

        pthread_mutex_unlock(&is->video_mutex);

        if (!ret)
        {
            // Process the frame
            send_frame(frame, is, sws_ctx);
        }
    }
    return 0;
}

void *feeding(void *arg)
{
    videostate *is = arg;
    AVPacket packet, *pkt = &packet;
    int ret = 0;

    while ((ret = packet_queue_get(&is->videoq, pkt, 1)) >= 0)
    {
        pthread_mutex_lock(&is->video_mutex);

        // send packet to decode
        ret = avcodec_send_packet(is->avctx, pkt);
        if (!ret)
        {
            pthread_cond_signal(&is->video_cond);
        }
        else if (ret == AVERROR(EAGAIN))
        {
            // Signal the draining loop
            pthread_cond_signal(&is->video_cond);
            // Wait here
            pthread_cond_wait(&is->video_cond, &is->video_mutex);
        }
        else if (ret < 0)
        {
            pthread_mutex_unlock(&is->video_mutex);
            return 0;
        }
        pthread_mutex_unlock(&is->video_mutex);
    }
    pthread_mutex_lock(&is->video_mutex);

    ret = avcodec_send_packet(is->avctx, NULL);

    pthread_cond_signal(&is->video_cond);
    pthread_mutex_unlock(&is->video_mutex);
    return 0;
}
