#include "../include/packet_queue.h"

#include <pthread.h>
#include <libavformat/avformat.h>

extern int quit;

void packet_queue_init(PacketQueue *q)
{
    memset(q, 0, sizeof(PacketQueue));

    if (pthread_mutex_init(&q->mutex, NULL) != 0)
    {
        perror("pthread_mutex_init() error");
        exit(EXIT_FAILURE);
    }
    if (pthread_cond_init(&q->cond, NULL) != 0)
    {
        perror("pthread_cond_init() error");
        exit(EXIT_FAILURE);
    }
}

int packet_queue_put(PacketQueue *q, AVPacket *pkt)
{
    AVPacketList *pkt_node;

    if (av_dup_packet(pkt) < 0)
    {
        return -1;
    }
    pkt_node = av_mallocz(sizeof(AVPacketList));
    if (!pkt_node)
    {
        return -1;
    }

    pkt_node->pkt = *pkt;
    pkt_node->next = NULL;

    pthread_mutex_lock(&q->mutex);

    if (!q->last_pkt)
    {
        q->first_pkt = pkt_node;
    }
    else
    {
        q->last_pkt->next = pkt_node;
    }
    q->last_pkt = pkt_node;
    q->nb_packets++;
    q->size += pkt_node->pkt.size;
    pthread_cond_signal(&q->cond);

    pthread_mutex_unlock(&q->mutex);
    return 0;

}

int packet_queue_get(PacketQueue *q, AVPacket *pkt, int block)
{
    AVPacketList *pkt_node;
    int ret;

    pthread_mutex_lock(&q->mutex);

    for (;;)
    {
        if (quit)
        {
            ret = -1;
            break;
        }

        pkt_node = q->first_pkt;
        if (pkt_node)
        {
            q->first_pkt = q->first_pkt->next;
            if (!q->first_pkt)
            {
                q->last_pkt = NULL;

            }
            q->nb_packets--;
            q->size -= pkt_node->pkt.size;
            *pkt = pkt_node->pkt;
            av_free(pkt_node);
            ret = 1;
            break;
        }
        else if (!block)
        {
            ret = 0;
            break;
        }
        else
        {
            pthread_cond_wait(&q->cond, &q->mutex);
        }
    }

    pthread_mutex_unlock(&q->mutex);
    return ret;
}
